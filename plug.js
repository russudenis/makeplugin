const errors = {
    error: 'You have an error'
}


class SimpleMethods {
    constructor() {}

    addClass(selector, cls) {
        if (!selector) {
            return console.log('Give an selector')
        }
        var element = document.querySelectorAll(selector);
        for (let i = 0; i < element.length; i++) {
            element[i].classList.add(cls)
        }
    }
    insertHTML(selector, text) {
        if (!selector) {
            return
        }
        var element = document.querySelectorAll(selector);
        for (let i = 0; i < element.length; i++) {
            element[i].innerHTML = text
        }
    }

}

var simple = new SimpleMethods();